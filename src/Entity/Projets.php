<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 06/03/2018
 * Time: 18:32
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="projets")
 * @ORM\Entity
 */
class Projets
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string $detail
     *
     * @ORM\Column(name="detail", type="string")
     */
    private $detail;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;
	/**
	 * @var string $tag
	 *
	 * @ORM\Column(name="tag", type="string", length=255)
	 */
	private $tag;

    public function getId() {
        return $this->id;
    }
    public function getNom() {
        return $this->nom;
    }
    public function getType() {
        return $this->type;
    }
    public function getDetail() {
        return $this->detail;
    }
    public function getImage() {
        return $this->image;
    }
	public function getTag() {
		return $this->tag;
	}
}