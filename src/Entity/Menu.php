<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="menu")
 * @ORM\Entity
 */
class Menu
{
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var string $item
	 *
	 * @ORM\Column(name="item", type="string", length=255)
	 */
	private $item;

	/**
	 * @var integer $dest
	 *
	 * @ORM\Column(name="destination", type="string", length=255)
	 */
	private $dest;



	public function getId() {
		return $this->id;
	}
	public function getItem() {
		return $this->item;
	}
	public function getDest() {
		return $this->dest;
	}

	public function setItem($item) {
		$this->item=$item;
	}
	public function setDest($dest) {
		$this->dest=$dest;
	}
}