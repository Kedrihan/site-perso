<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
setlocale(LC_TIME,'fr_FR');
/**
 * @ORM\Table(name="formation")
 * @ORM\Entity
 */
class Formation
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $ecole
     *
     * @ORM\Column(name="ecole", type="string", length=255)
     */
    private $ecole;

    /**
     * @var string $diplome
     *
     * @ORM\Column(name="diplome", type="string", length=255)
     */
    private $diplome;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var string $dateDebut
     *
     * @ORM\Column(name="dateDebut", type="date")
     */
    private $dateDebut;

    /**
     * @var string $dateFin
     *
     * @ORM\Column(name="dateFin", type="date")
     */
    private $dateFin;

    /**
     * @var string $info_supp
     *
     * @ORM\Column(name="info_supp", type="string", options={"default":null})
     */
    private $info_supp;

    /**
     * @var string $type_diplome
     *
     * @ORM\Column(name="type_diplome", type="string")
     */
    private $type_diplome;


    public function getId() {
        return $this->id;
    }
    public function getEcole() {
        return $this->ecole;
    }
    public function getDiplome() {
        return $this->diplome;
    }
    public function getDescri() {
        return $this->description;
    }
    public function getInfosupp() {
        $infosupp = $this->info_supp;
        return $infosupp;
    }
    public function getType() {
        $type=$this->type_diplome;
        return $type;
    }



    public function getFin() {
        $d = (int)date_format($this->dateFin,'U');

        if ($d >= 0) {
            $fin = utf8_encode(strftime( '%Y', $d ));
        }
        else {
            $fin = '';
        }
        return $fin;
    }
    public function getDebut() {
        $d = (int)date_format($this->dateDebut,'U');
        $debut = utf8_encode(strftime('%Y',$d));

        return $debut;
    }

    public function setEcole($ecole) {
        $this->ecole=$ecole;
    }
    public function setDiplome($diplome) {
        $this->diplome=$diplome;
    }
    public function setDescr($descr) {
        $this->description=$descr;
    }
    public function setDebut($debut) {
        $this->dateDebut=$debut;
    }
    public function setFin($fin) {
        $this->dateFin=$fin;
    }
}