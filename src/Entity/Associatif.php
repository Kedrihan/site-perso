<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
setlocale(LC_TIME,'fr_FR');
/**
 * @ORM\Table(name="associatif")
 * @ORM\Entity
 */
class Associatif
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $asso
     *
     * @ORM\Column(name="asso", type="string", length=255)
     */
    private $asso;

    /**
     * @var string $poste
     *
     * @ORM\Column(name="poste", type="string", length=255)
     */
    private $poste;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var string $dateDebut
     *
     * @ORM\Column(name="dateDebut", type="date")
     */
    private $dateDebut;

    /**
     * @var string $dateFin
     *
     * @ORM\Column(name="dateFin", type="date")
     */
    private $dateFin;


    public function getId() {
        return $this->id;
    }
    public function getAsso() {
        return $this->asso;
    }
    public function getPoste() {
        return $this->poste;
    }
    public function getDescri() {
        return $this->description;
    }



    public function getFin() {
        if ($this->dateFin===null) {
            return null;
        }
        else {
            $d = (int)date_format($this->dateFin, 'U');

            if ($d >= 0) {
                $fin = utf8_encode(strftime('%b %Y', $d));
            } else {
                $fin = '';
            }
            return $fin;
        }
    }
    public function getDebut() {
        $d = (int)date_format($this->dateDebut,'U');
        $debut = utf8_encode(strftime('%b %Y',$d));

        return $debut;
    }

    public function setAsso($asso) {
        $this->asso=$asso;
    }
    public function setPoste($poste) {
        $this->poste=$poste;
    }
    public function setDescr($descr) {
        $this->description=$descr;
    }
    public function setDebut($debut) {
        $this->dateDebut=$debut;
    }
    public function setFin($fin) {
        $this->dateFin=$fin;
    }
}