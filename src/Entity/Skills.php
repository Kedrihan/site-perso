<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="skills")
 * @ORM\Entity
 */
class Skills
{
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var string $skill
	 *
	 * @ORM\Column(name="skill", type="string", length=255)
	 */
	private $skill;

	/**
	 * @var integer $percent
	 *
	 * @ORM\Column(name="percent", type="integer")
	 */
	private $percent;



	public function getId() {
	    return $this->id;
    }
    public function getSkill() {
        return $this->skill;
    }
    public function getPercent() {
        return $this->percent;
    }

    public function setSkill($skill) {
        $this->skill=$skill;
    }
    public function setPercent($percent) {
        $this->percent=$percent;
    }
}