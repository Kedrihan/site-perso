<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


use Symfony\Component\Routing\Annotation\Route;


class TBCController extends AbstractController
{
    /**
     * Matches /bot-cruella
     *
     * @Route("/bot-cruella", name="bc")
     */
    public function indexAction(Request $request)
    {
        $isActif = shell_exec('pgrep -lf js');
        if (strpos($isActif, 'node') !== false) {
            $actif = true;
            $pid = explode(' ',trim($isActif))[0];
        } else {
            $actif = false;
            $pid = null;
        }

        $activateP = new Process("/root/.nvm/versions/node/v10.5.0/bin/node /var/www/html/bot-twitch-cruella/bot.js &");
        $deactivateP = new Process('kill '.$pid);
        $formA = $this->createFormBuilder()
            ->add('hidden', HiddenType::class, array('attr' => ['value' => 'activate']))
            ->add('submit', SubmitType::class, array('label' => 'Activer le bot'))->getForm();
        $formD = $this->createFormBuilder()
            ->add('hidden', HiddenType::class, array('attr' => ['value' => 'deactivate']))
            ->add('submit', SubmitType::class, array('label' => 'D�sactiver le bot'))->getForm();
        $formA->handleRequest($request);
        $formD->handleRequest($request);
        if ($formA->isSubmitted() && $formA->isValid()) {
            if (!$actif) {
                try {
                    $activateP->run();

                    if (!$activateP->isSuccessful()) {
                        throw new ProcessFailedException($activateP);
                    }
                    $isActif = shell_exec('pgrep -lf js');
                    if (strpos($isActif, 'node') !== false) {
                        $actif = true;
                        $pid = explode(' ',trim($isActif))[0];
                    } else {
                        $actif = false;
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
        if ($formD->isSubmitted() && $formD->isValid()) {
            if ($actif) {
                try {
                    $deactivateP->run();

                    if (!$deactivateP->isSuccessful()) {
                        throw new ProcessFailedException($deactivateP);
                    }
                    $isActif = shell_exec('pgrep -lf js');
                    if (strpos($isActif, 'node') !== false) {
                        $actif = true;
                        $pid = explode(' ',trim($isActif))[0];
                    } else {
                        $actif = false;
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
        return $this->render(
            'bot.html.twig',
            array(
                'formA' => $formA->createView(),
                'formD' => $formD->createView(),
                'actif' => $actif,
            )
        );
    }
}