<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Mobile_Detect;

use App\Entity\Skills;
use App\Entity\Projets;
use App\Entity\Contact;
use App\Entity\Menu;
use App\Entity\Associatif;
use App\Entity\Experience;
use App\Entity\Formation;
use \DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Tests\Fixtures\ToString;


class CVController extends AbstractController {
	/**
	 * Matches /corentin-faure
	 *
	 * @Route("/corentin-faure", name="cv")
	 */
	public function indexAction( Request $request, \Swift_Mailer $mailer ) {
		$detect = new Mobile_Detect;
		// creates a task and gives it some dummy data for this example
		$contact = new Contact();

		$form = $this->createFormBuilder( $contact )
		             ->add( 'nom', TextType::class, array(
			             'attr' => array(
				             'class'       => 'form-control',
				             'name'        => 'name',
				             'required'    => 'required',
				             'placeholder' => 'Nom'
			             )
		             ) )
		             ->add( 'email', EmailType::class, array(
			             'attr' => array(
				             'class'       => 'form-control',
				             'name'        => '_replyto',
				             'required'    => 'required',
				             'placeholder' => 'E-mail'
			             )
		             ) )
		             ->add( 'sujet', TextType::class, array(
			             'attr' => array(
				             'class'       => 'form-control',
				             'name'        => 'Subject',
				             'required'    => 'required',
				             'placeholder' => 'Sujet'
			             )
		             ) )
		             ->add( 'message', TextareaType::class, array(
			             'attr' => array(
				             'class'       => 'form-control',
				             'name'        => 'message',
				             'required'    => 'required',
				             'placeholder' => 'Votre Message'
			             )
		             ) )
		             ->add( 'Save', SubmitType::class, array(
			             'label' => 'Envoyer',
			             'attr'  => array( 'class' => 'btn btn-primary' )
		             ) )
		             ->getForm();
		$form->handleRequest( $request );

		try {
			if ( $form->isSubmitted() && $form->isValid() ) {
				// $form->getData() holds the submitted values
				// but, the original `$task` variable has also been updated
				$contact = $form->getData();
				$message = ( new \Swift_Message( $contact->getSujet() ) )
					->setFrom( $contact->getEmail() )
					->setTo( 'corentin.faure@outlook.com' )
					->setBody( 'De la part de : ' . $contact->getNom() . PHP_EOL . 'Adresse mail : ' . $contact->getEmail() . PHP_EOL . PHP_EOL . PHP_EOL . 'Message : ' . $contact->getMessage() );
				$message->setContentType( "text/plain" );
				if ( $mailer->send( $message ) ) {
					$this->addFlash(
						'notice',
						'Envoi effectué !'
					);
				} else {
					$this->addFlash(
						'error',
						'Erreur, veuillez rééssayer'
					);
				}

				return $this->redirect( '/corentin-faure#contact' );
			}
		}
		catch(\Exception $exception) {
			$this->addFlash(
				'error',
				'Une erreur est arrivée : '.$exception->getMessage()
			);
			return $this->redirect( 'corentin-faure#contact' );
		}
		$defaut = DateTime::createFromFormat( '1111-11-11', '' );
		$repo   = $this->getDoctrine()->getRepository( Skills::class );
		$skills = $repo->findAll();

		$repo = $this->getDoctrine()->getRepository( Menu::class );
		$menu = $repo->findAll();

		$repo       = $this->getDoctrine()->getRepository( Experience::class );
		$experience = $repo->findBy( array(), array( 'id' => 'DESC' ) );

		$repo = $this->getDoctrine()->getRepository( Associatif::class );
		$asso = $repo->findBy( array(), array( 'id' => 'DESC' ) );

		$repo      = $this->getDoctrine()->getRepository( Formation::class );
		$formation = $repo->findBy( array(), array( 'id' => 'DESC' ) );
		if ( $detect->isMobile() && ! $detect->isTablet() ) {
			foreach ( $formation as $elem ) {
				$elem->infosupp = null;
			}

		}

		$repo    = $this->getDoctrine()->getRepository( Projets::class );
		$projets = $repo->findBy( array(), array( 'id' => 'DESC' ) );

		$anniv   = new DateTime( '1996-06-20' );
		$current = new DateTime( date( 'Y-M-d' ) );
		$ageint     = $anniv->diff( $current );
		$age = $ageint->format('%y');

		return $this->render(
			'base.html.twig',
			array(
				'skills'     => $skills,
				'rows'       => 2,
				'menu'       => $menu,
				'experience' => $experience,
				'associatif' => $asso,
				'formation'  => $formation,
				'defaut'     => $defaut,
				'form'       => $form->createView(),
				'projets'    => $projets,
				'age'        => $age
			)
		);
	}
}