<?php
/**
 * Created by PhpStorm.
 * User: cfaure
 * Date: 16/11/2018
 * Time: 09:39
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use TwitchApi\Api\Authentication;

class KedrihanController extends AbstractController
{
    /**
     * Matches /test
     *
     * @Route("/test", name="test")
     */
    public function indexAction(\Symfony\Component\HttpFoundation\Request $req)
    {


        return $this->render('kedrihan.html.twig', array());
    }

}