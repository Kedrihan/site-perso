<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Skills;
use App\Entity\Menu;
use App\Entity\Associatif;
use App\Entity\Experience;
use App\Entity\Formation;
use \DateTime;
use Symfony\Component\Routing\Annotation\Route;
class DefaultController extends AbstractController
{
    /**
     * Matches /
     *
     * @Route("/", name="index")
     */
	public function indexAction()
	{
		return $this->redirect('corentin-faure');

	}
}