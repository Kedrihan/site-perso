<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Tests\Fixtures\ToString;


class PunitionController extends AbstractController
{
    /**
     * Matches /cruella-est-gentille
     *
     * @Route("/cruella-est-gentille", name="lol")
     */
    public function indexAction()
    {
        $fill = "Cruella est gentille ";
        for ($i = 0; $i < 20; $i++) {
            $fill .= $fill;
        }
        return $this->render(
            'ptdr.html.twig',
            array(
                'lol' => $fill
            )
        );
    }
}