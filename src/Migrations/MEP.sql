INSERT INTO experience (entreprise,poste,description,dateDebut)
  VALUES ('IGC Services (Groupe Casino)', 'Agent administratif', 'Classement de dossiers administratifs et réorganisation de la classification des archives du service.','2016-08-01');

INSERT INTO experience (entreprise,poste,description,dateDebut,dateFin)
  VALUES ('Distribution Casino France', 'Stagiaire', 'Stage dans le cadre du DUT GEII.<br> Missions : <br>- Étude de faisabilité de la migration du parc informatique sous Windows 10<br>- Mise en œuvre et test en pilote de Windows 10 dans le contexte de l''entreprise <br>- Développement de scripts PowerShell communiquant avec l''Active Directory du Groupe','2017-04-14','2017-06-30');

INSERT INTO experience (entreprise,poste,description,dateDebut,dateFin)
  VALUES ('Gfi Informatique', 'Concepteur développeur alternant', 'En alternance à Sciences-U Lyon.<br> Travail et développement des compétences sur différentes technologies : <br> - ASP.NET MVC<br>- KendoUI<br>- Entity Framework<br>- SQL Server<br>- Node.js','2017-09-18','2018-09-14');

INSERT INTO associatif (asso,poste,description,dateDebut,dateFin)
  VALUES ('Association Metaleak', 'Responsable Gaming pôle Overwatch*', '- Recrutement et gestion d''équipes de sport électronique, développement de la communauté gravitant autour de ces dernières<br>- Organisation, administration et gestion de tournois de jeux vidéos en ligne.<br><br><br><br><i>*Overwatch : jeu vidéo créé par Blizzard Entertainment</i>','2016-11-01','2017-10-01');

INSERT INTO associatif (asso,poste,description,dateDebut,dateFin)
  VALUES ('orKs eSports', 'Manager section PUBG* et streamer', '- Management d''équipes de sport électronique (gestion du planning d''entraînement, inscriptions aux tournois, gestion des conflits internes)<br>- Diffusion en direct de jeux sur la plateforme Twitch au sein de la Web TV de l''association <br><br><br><br><i>*PUBG : PlayerUnknown''s Battlegrounds, jeu vidéo créé par BlueHole</i>','2017-10-02','1111-11-11');

INSERT INTO formation (ecole,diplome,description,dateDebut,dateFin, type_diplome)
  VALUES ('Lycée François Mauriac Forez, Andrézieux-Bouthéon', 'Baccalauréat Scientifique - Sciences de l''Ingénieur', 'Option Informatique et Sciences du Numérique, mention Assez Bien','2011-09-02','2014-07-02','Lycée');

INSERT INTO formation (ecole,diplome,description,dateDebut,dateFin,info_supp, type_diplome)
  VALUES ('Télécom Saint Etienne', 'DUT Génie électrique et informatique industrielle (GEII)', 'Cycle initial en technologie de l’information de Saint-Etienne (CITISE), mêlant enseignements pratiques via l''IUT de Saint Etienne et enseignements théoriques via l''école d''ingénieurs Télécom Saint Etienne.','2014-09-02','2017-07-02','files/citise.pdf','DUT');

INSERT INTO formation (ecole,diplome,description,dateDebut,dateFin,info_supp,type_diplome)
  VALUES ('Sciences-U Lyon', 'Bachelor Concepteur Réalisateur Web & Digital', 'Titre de niveau II "Concepteur réalisateur web et digital" inscrit au RNCP (JO du 27/05/2015)<br> En alternance chez Gfi Informatique.','2017-09-18','2018-09-14','files/crwd.pdf','Bachelor');

INSERT INTO skills (skill, percent) VALUES ('HTML / CSS', '80'), ('PHP', '90'), ('Symfony', '50'), ('ASP.NET MVC', '60'), ('Javascript', '15'), ('SGBD (MySQL, PostgreSQL, SQL Server)', '90'), ('SQL', '95'), ('C#, C, C++', '85'), ('SEO', '30'), ('Linux, Windows', '75');

INSERT INTO associatif (asso,poste,description,dateDebut)
  VALUES ('Presqu''Anims', 'Staff Clermont Geek Convention', 'Membre de l''équipe du staff de la convention Clermont Geek Convention &agrave; Clermont-Ferrand les 17 et 18 Mars 2018<br><br>Tâche principale : gestion des files d''attentes pour les dédicaces des invités','2018-03-17');
update associatif set description='Membre de l''&eacute;quipe du staff de la convention Clermont Geek Convention &agrave; Clermont-Ferrand les 17 et 18 Mars 2018<br><br>T&acirc;che principale : gestion des files d''attentes pour les d&eacute;dicaces des invit&eacute;s',dateFin='0000-00-00' where asso='Presqu''Anims';

INSERT INTO projets (nom,`type`,detail,image)
  VALUES ('Projet Web Sciences-U', 'Développement web', 'hrdthdrth','images/project-1.jpg');
INSERT INTO projets (nom,`type`,detail,image)
  VALUES ('Marketplace', 'Développement web', 'hrdthdrth','images/project-2.jpg');
INSERT INTO projets (nom,`type`,detail,image)
  VALUES ('Projet C# DUT GEII', 'Développement logiciel', 'hrdthdrth','images/project-3.jpg');
INSERT INTO projets (nom,`type`,detail,image)
  VALUES ('Projet C++ DUT GEII', 'Développement console', 'hrdthdrth','images/project-4.jpg');

ALTER TABLE `projets` ADD `tag` VARCHAR(255) NOT NULL AFTER `image`;

UPDATE `projets` SET `tag` = 'scu' WHERE `projets`.`id` = 1; UPDATE `projets` SET `tag` = 'market' WHERE `projets`.`id` = 2; UPDATE `projets` SET `tag` = 'csdut' WHERE `projets`.`id` = 3; UPDATE `projets` SET `tag` = 'cpdut' WHERE `projets`.`id` = 4;
ALTER TABLE `projets` CHANGE `detail` `detail` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
UPDATE `projets` SET `detail` = 'Projet de gestion de stock de magasin : <br>Gestion d''une base de données, exportation des données en CSV.<br><br>Outils : C# (Windows Forms), PostgreSQL' WHERE `projets`.`id` = 3;
UPDATE `projets` SET `detail` = 'Projet de création de site web de location d''instruments de musique entre particuliers.<br>Livrables :<br>- Étude d’opportunité<br>- Cahier des charges fonctionnel<br>- Dossier de spécifications techniques<br>- Dossier de stratégie marketing + Plan de communication<br>- Dossier de réalisation<br>- La/les solutions techniques.<br><br>Technologies et outils utilisées : PHP Symfony 4, Bootstrap 4, MySQL, Git, Trello' WHERE `projets`.`id` = 1;
UPDATE `projets` SET `detail` = 'Chef de projet sur la création d''un programme en C++ permettant à partir de paramètres d''entrée de calculer l''ensoleillement en un point donné de la planète.<br>- Découpage fonctionnel à partir d''un cahier des charges<br>- Constitution de groupes<br>- Répartition des tâches dans ces groupes<br>- Tests et rédaction de rapport suivi d''une soutenance' WHERE `projets`.`id` = 4;
UPDATE `projets` SET `detail` = 'Projet de marketplace avec la plateforme Sharetribe opensource.<br>Outils : <br>- Ruby on Rails<br>- MySQL<br>- React<br>- jQuery<br>- Node.js' WHERE `projets`.`id` = 2;

UPDATE associatif SET asso='orKs Grand Poitiers' where asso='orKs eSports';

INSERT INTO menu (item,destination)
  VALUES ('A propos', '#about');
INSERT INTO menu (item,destination)
  VALUES ('Compétences', '#skill');
INSERT INTO menu (item,destination)
  VALUES ('Projets', '#projets');
INSERT INTO menu (item,destination)
  VALUES ('Expérience', '#experience');
INSERT INTO menu (item,destination)
  VALUES ('Contact', '#contact');