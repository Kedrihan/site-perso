cc:
    php bin/console cache:clear
    chown -R www-data:www-data var/
update-prod:
    git pull
    composer update -o
    make cc