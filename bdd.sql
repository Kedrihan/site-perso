
INSERT INTO projets (nom,type,detail,image,tag) VALUES ("Projet C++ DUT GEII","Developpement console","Chef de projet sur la cr&eacute;ation d'un programme en C++ permettant &agrave; partir de param&egrave;tres d'entr&eacute;e de calculer l'ensoleillement en un point donn&eacute; de la plan&egrave;te.<br>- D&eacute;coupage fonctionnel &agrave; partir d'un cahier des charges<br>- Constitution de groupes<br>- R&eacute;partition des tâches dans ces groupes<br>- Tests et r&eacute;daction de rapport suivi d'une soutenance de projet","images/project-4.jpg","cpdut");
INSERT INTO projets (nom,type,detail,image,tag) VALUES ("Projet C# DUT GEII","Developpement logiciel","Projet de gestion de stock de magasin :<br>Gestion d'une base de donn&eacute;es, exportation des donn&eacute;es en CSV.<br>Outils : C# (Windows Forms), PostgreSQL","images/project-3.jpg","csdut");
INSERT INTO projets (nom,type,detail,image,tag) VALUES ("Projet Web Sciences-U","Developpement web","Projet en cours<br><br>Projet de cr&eacute;ation de site web de location d'instruments de musique entre particuliers.<br>Livrables :<br>- &eacute;tude d'opportunit&eacute;<br>- Cahier des charges fonctionnel<br>- Dossier de sp&eacute;cifications techniques<br>- Dossier de strat&eacute;gie marketing + Plan de communication<br>- Dossier de r&eacute;alisation<br>- La/les solutions techniques.<br><br>Technologies et outils utilis&eacute;es : Symfony 4, Bootstrap 4, MySQL, Git, Trello","images/project-1.jpg","scu");
INSERT INTO projets (nom,type,detail,image,tag) VALUES ("Refonte du site *aAa*","D&egrave;veloppement web","Projet en cours <br><br>Refonte compl&eacute;te du <a href=\"http://www.team-aaa.com\">site web *aAa*</a> :<br>- Passage d'un site from scratch &agrave; un site sous Symfony<br>- Site &agrave; fort traffic","images/project-aaa.jpg","aaa");




UPDATE projets SET image="images/project-1.jpg" WHERE id=1;
UPDATE projets SET image="images/project-3.jpg" WHERE id=3;
UPDATE projets SET image="images/project-4.jpg" WHERE id=4;
UPDATE projets SET image="images/project-aaa.jpg" WHERE id=5;
UPDATE projets SET type="Developpement web" WHERE id=1;
UPDATE projets SET type="Developpement logiciel" WHERE id=3;
UPDATE projets SET type="Developpement console" WHERE id=4;
UPDATE projets SET type="Developpement web" WHERE id=5;

UPDATE projets SET detail="Projet en cours <br><br>Refonte compl&eacute;te du <a href=\"http://www.team-aaa.com\">site web *aAa*</a> :<br>- Passage d'un site from scratch &agrave; un site sous Symfony<br>- Site &agrave; fort trafic" where id=4;

UPDATE projets SET id=1 where