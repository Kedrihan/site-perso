-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: siteperso
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `associatif`
--

DROP TABLE IF EXISTS `associatif`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `associatif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `poste` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `associatif`
--

LOCK TABLES `associatif` WRITE;
/*!40000 ALTER TABLE `associatif` DISABLE KEYS */;
INSERT INTO `associatif` VALUES (1,'Association Metaleak','Responsable Gaming pôle Overwatch*','- Recrutement et gestion d\'&eacute;quipes de sport &eacute;lectronique, d&eacute;veloppement de la communaut&eacute; gravitant autour de ces derni&egrave;res<br>- Organisation, administration et gestion de tournois de jeux vid&eacute;os en ligne.<br><br><br><br><i>*Overwatch : jeu vid&eacute;o cr&eacute;&eacute; par Blizzard Entertainment</i>','2016-11-01','2017-10-01'),(2,'orKs Grand Poitiers','Manager section PUBG* et streamer','- Management d\'&eacute;quipes de sport &eacute;lectronique (gestion du planning d\'entra&icirc;nement, inscriptions aux tournois, gestion de la logistique pour les tournois LAN, gestion des conflits internes)<br>- Mise en avant de l\'&eacute;quipe pour la recherche de sponsors<br>- Diffusion en direct de jeux sur la plateforme Twitch au sein de la Web TV de l\'association <br><br><br><br><i>*PUBG : PlayerUnknown\'s Battlegrounds, jeu vid&eacute;o cr&eacute;&eacute; par BlueHole</i>','2017-10-02','2018-11-25'),(3,'Presqu\'Anims','Staff Clermont Geek Convention','Membre de l\'&eacute;quipe du staff de la convention Clermont Geek Convention &agrave; Clermont-Ferrand les 17 et 18 Mars 2018<br><br>T&acirc;che principale : gestion des files d\'attentes pour les d&eacute;dicaces des invit&eacute;s','2018-03-17','2018-03-18');
/*!40000 ALTER TABLE `associatif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience`
--

DROP TABLE IF EXISTS `experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entreprise` varchar(255) NOT NULL,
  `poste` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience`
--

LOCK TABLES `experience` WRITE;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` VALUES (1,'IGC Services (Groupe Casino)','Agent administratif','Classement de dossiers administratifs et réorganisation de la classification des archives du service.','2016-08-01','2016-08-02'),(2,'Distribution Casino France','Stagiaire','Stage dans le cadre du DUT GEII.<br> Missions : <br>- Étude de faisabilité de la migration du parc informatique sous Windows 10<br>- Mise en œuvre et test en pilote de Windows 10 dans le contexte de l\'entreprise <br>- Développement de scripts PowerShell communiquant avec l\'Active Directory du Groupe','2017-04-14','2017-06-30'),(3,'Gfi Informatique','Concepteur développeur alternant','En alternance &agrave; Sciences-U Lyon.<br>\nTravail et d&eacute;veloppement de mes comp&eacute;tences principalement sur la technologie .Net (Core & MVC) :<br>\n- D&eacute;veloppement d\'une API interne pour am&eacute;liorer le reporting des projets (.Net Core), en regroupant les informations des diff&eacute;rents outils d&eacute;j&agrave; pr&eacute;sents<br>\n- D&eacute;veloppement d\'&eacute;volutions sur plusieurs applications client (.Net MVC)<br>\n- &Eacute;tude des PWA (Progressive Web App) afin d\'en ressortir les avantages et les inconv&eacute;nients<br><br>\nMais &eacute;galement am&eacute;lioration de mon travail en &eacute;quipe (m&eacute;thode agile SCRUM) et de ma rigueur.','2017-09-18','2018-09-14'),(4,'Intitek','Developpeur PHP/.Net Junior','- Mission de Octobre  D&eacute;cembre 2018 - Client : GiSmartware -- Correctifs et &eacute;volutions sur un client lourd WPF','2018-10-01','1111-11-11');
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formation`
--

DROP TABLE IF EXISTS `formation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ecole` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diplome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date DEFAULT NULL,
  `info_supp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_diplome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formation`
--

LOCK TABLES `formation` WRITE;
/*!40000 ALTER TABLE `formation` DISABLE KEYS */;
INSERT INTO `formation` VALUES (1,'Lycée François Mauriac Forez, Andrézieux-Bouthéon','Baccalauréat Scientifique - Sciences de l\'Ingénieur','Option Informatique et Sciences du Numérique, mention Assez Bien','2011-09-02','2014-07-02','','Lycée'),(2,'Télécom Saint Etienne','DUT Génie électrique et informatique industrielle (GEII)','Cycle initial en technologie de l’information de Saint-Etienne (CITISE), mêlant enseignements pratiques via l\'IUT de Saint Etienne et enseignements théoriques via l\'école d\'ingénieurs Télécom Saint Etienne.','2014-09-02','2017-07-02','files/citise.pdf','DUT'),(3,'Sciences-U Lyon','Bachelor Concepteur Réalisateur Web & Digital','Titre de niveau II \"Concepteur réalisateur web et digital\" inscrit au RNCP (JO du 27/05/2015)<br> En alternance chez Gfi Informatique.','2017-09-18','2018-09-14','files/crwd.pdf','Bachelor');
/*!40000 ALTER TABLE `formation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'A propos','#about'),(2,'Comp&eacute;tences','#skill'),(3,'Projets','#projets'),(4,'Exp&eacute;rience','#experience'),(5,'Contact','#contact');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projets`
--

DROP TABLE IF EXISTS `projets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projets`
--

LOCK TABLES `projets` WRITE;
/*!40000 ALTER TABLE `projets` DISABLE KEYS */;
INSERT INTO `projets` VALUES (1,'Projet C++ DUT GEII','Developpement console','Chef de projet sur la cr&eacute;ation d\'un programme en C++ permettant &agrave; partir de param&egrave;tres d\'entr&eacute;e de calculer l\'ensoleillement en un point donn&eacute; de la plan&egrave;te.<br>- D&eacute;coupage fonctionnel &agrave; partir d\'un cahier des charges<br>- Constitution de groupes<br>- R&eacute;partition des tches dans ces groupes<br>- Tests et r&eacute;daction de rapport suivi d\'une soutenance de projet','images/project-4.jpg','cpdut'),(2,'Projet C# DUT GEII','Developpement logiciel','Projet de gestion de stock de magasin :<br>Gestion d\'une base de donn&eacute;es, exportation des donn&eacute;es en CSV.<br>Outils : C# (Windows Forms), PostgreSQL','images/project-3.jpg','csdut'),(3,'Projet Web Sciences-U','Developpement web','Projet de cr&eacute;ation de site web de location d\'instruments de musique entre particuliers.<br>Livrables :<br>- &eacute;tude d\'opportunit&eacute;<br>- Cahier des charges fonctionnel<br>- Dossier de sp&eacute;cifications techniques<br>- Dossier de strat&eacute;gie marketing + Plan de communication<br>- Dossier de r&eacute;alisation<br>- La/les solutions techniques.<br><br>Technologies et outils utilis&eacute;es : Symfony 4, Bootstrap 4, MySQL, Git, Trello','images/project-1.jpg','scu');
/*!40000 ALTER TABLE `projets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,'HTML / CSS',85),(2,'PHP',90),(3,'Symfony (3 & 4)',80),(4,'.NET (Core & MVC)',75),(5,'Javascript',70),(6,'SGBD (MySQL, PostgreSQL, SQL Server)',90),(7,'SQL',95),(8,'C#, C, C++',85),(9,'Git',85),(10,'Linux, Windows',75);
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-14  9:49:55
